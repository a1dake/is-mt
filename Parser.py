from datetime import datetime
import urllib3
from bs4 import BeautifulSoup
import requests
import cfscrape
import sys
import os
import sqlite3
from urllib3 import exceptions
currentdir = os.path.dirname(os.path.realpath(__file__))
db_path = os.path.join(currentdir, "is-mt.db")
sys.path.append("/home/manage_report")

from Send_report.mywrapper import magicDB

class Parser:
    def __init__(self, parser_name: str):
        self.session = cfscrape.create_scraper(sess=requests.Session())
        self.result_data: dict = {'name': parser_name,
                                  'data': []}

    @magicDB
    def run(self):
        content: list = self.get_content()
        self.result_data['data'] = content
        print(content)
        return self.result_data

    def create_database(self):
        try:
            conn = sqlite3.connect(db_path)
            sql = conn.cursor()
            sql.execute("""CREATE TABLE orders (url text);""")
            conn.close()
        except:
            pass

    def insert_data(self, value):
        conn = sqlite3.connect(db_path)
        c = conn.cursor()
        c.execute("INSERT INTO orders(url) VALUES (?)", [value])
        conn.commit()
        conn.close()

    def search_data(self, search_value):
        conn = sqlite3.connect(db_path)
        c = conn.cursor()
        c.execute("SELECT * FROM orders WHERE url = ?", (search_value,))
        rows = c.fetchall()
        if rows:
            conn.close()
            return True
        else:
            conn.close()
            return False

    def get_urls(self):
        self.session.headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'User-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36 Edg/108.0.1462.54',
        }
        ads = []

        url = 'http://is-mt.pro/Purchase/ListPurchase'
        response = self.session.get(url, timeout=20)
        soup = BeautifulSoup(response.content, "html.parser")

        for tr in soup.find_all("tr")[1:]:
            tender_link = 'http://is-mt.pro' + ''.join(tr.find_all("td")[2].find("a").get("href")).strip()
            db_search = self.search_data(tender_link)
            if db_search:
                continue
            else:
                ads.append(tender_link)
                self.insert_data(tender_link)
        return ads

    def get_content(self):
        self.create_database()
        ads = self.get_urls()
        contents = []
        for link in ads:
            item_data = {
                'type': 2,
                'title': '',
                'purchaseNumber': '',
                'fz': 'Коммерческие',
                'purchaseType': 'Запрос цен',
                'url': '',
                'lots': [],
                'procedureInfo': {
                    'endDate': ''
                },

                'customer': {
                    'fullName': '',
                    'factAddress': '',
                    'inn': 0,
                    'kpp': 0,
                },
                'contactPerson': {
                    'lastName': '',
                    'firstName': '',
                    'middleName': ''
                }
            }

            try:
                response = self.session.get(link, timeout=30)
                tree = BeautifulSoup(response.content, "html.parser")

                item_data['title'] = str(self.get_title(tree))
                item_data['url'] = str(link)
                item_data['purchaseNumber'] = str(self.get_number(link))

                customer_link = self.get_customer_link(tree)
                customer_data = self.get_customer_tree(customer_link)

                item_data['customer']['fullName'] = str(self.get_customer_name(customer_data))
                item_data['customer']['factAddress'] = str(self.get_customer_adress(customer_data))
                item_data['customer']['inn'] = int(self.get_customer_inn(customer_data))
                item_data['customer']['kpp'] = int(self.get_customer_kpp(customer_data))

                last_name, first_name, middle_name = self.get_names(tree)
                item_data['contactPerson']['lastName'] = str(last_name)
                item_data['contactPerson']['firstName'] = str(first_name)
                item_data['contactPerson']['middleName'] = str(middle_name)

                item_data['procedureInfo']['endDate'] = self.get_end_date(tree)

                item_data['lots'] = self.get_lots(link, tree)

                contents.append(item_data)
            except Exception as e:
                print(f"{e} - Страница с ошибкой ", link)
        return contents

    def get_title(self, soup):
        try:
            data = ''.join(soup.find("h1")).strip()
        except:
            data = ''
        return data

    def get_number(self, link):
        try:
            data = ''.join(link.split('?id=')[1])
        except:
            data = ''
        return data

    def get_customer_link(self, soup):
        try:
            data = soup.find_all("p")[9].find('a')['href']
        except:
            data = ''
        return data

    def get_customer_tree(self, link):
        try:
            urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
            customer_id = ''.join(link.split('/details/')[1])
            url = 'https://home.skotchapp.com/JuridicalPersonsSite/GetJuridicalPersonDetails?juridicalPersonId=' + customer_id
            self.session.headers = {
                'Accept': 'application/json, text/plain, */*',
                'User-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36 Edg/109.0.1518.70',
            }
            response = self.session.get(url, timeout=10, verify=False)
            data = response.json()
        except:
            data = ''
        return data

    def get_customer_name(self, data):
        try:
            data = data.get("data").get("FullName")
        except:
            data = ''
        return data

    def get_customer_adress(self, data):
        try:
            data = data.get("data").get("LegalAddress")
        except:
            data = ''
        return data

    def get_customer_inn(self, data):
        try:
            data = data.get("data").get("Inn")
            if not data or data is None:
                data = 0
        except:
            data = 0
        return data

    def get_customer_kpp(self, data):
        try:
            data = data.get("data").get("Kpp")
            if not data or data is None:
                data = 0
        except:
            data = 0
        return data

    def get_names(self, soup):
        try:
            contact = ''.join(''.join(''.join(soup.find_all("p")[11].get_text()).split('Контактное лицо:')[1]).split(' Задать вопрос')[0])

            try:
                last_name = ''.join(contact.split()[0])
            except:
                last_name = ''
            try:
                first_name = ''.join(contact.split()[1])
            except:
                first_name = ''
            try:
                middle_name = ''.join(contact.split()[2])
            except:
                middle_name = ''
        except:
            last_name = ''
            first_name = ''
            middle_name = ''
        return last_name, first_name, middle_name

    def get_end_date(self, soup):
        try:
            data = ''.join(''.join(''.join(soup.find_all("p")[6].get_text()).strip().split('Окончание закупки:')[1]).strip().split(' (')[0])
            formatted_date = self.formate_date(data)
        except:
            formatted_date = None
        return formatted_date

    def get_lots_tree(self, link):
        try:
            url = 'http://is-mt.pro/Purchase/ListCgc?id=' + ''.join(link.split('?id=')[1])
            response = self.session.get(url, timeout=30)
            tree = BeautifulSoup(response.content, "html.parser")
        except:
            tree = ''
        return tree

    def get_lots(self, link, tree):
        soup = self.get_lots_tree(link)
        try:
            data = []
            lots = {
                'region': '',
                'address': '',
                'price': '',
                'lotItems': []

            }
            lot_div = soup.find("table").find("tbody").find_all("tr")
            lot_code = 1
            lots['price'] = ''.join(''.join(str(tree.find("div", id="purchase_summary").find_all("p")[2].get_text()).split('Начальная стоимость: ')[1]).strip().split('\xa0'))
            for lot_item in lot_div:
                names = {'code': str(lot_code),
                         'name': ''.join(str(lot_item.find_all("td")[0].find("a").get_text()).strip())
                         }

                lots['lotItems'].append(names)
                lots['address'] = ' '.join(''.join(lot_item.find_all("td")[3].get_text()).strip().split())
                lot_code += 1
            data.append(lots)
        except:
            data = []
        return data

    def formate_date(self, old_date):
        date_object = datetime.strptime(old_date, '%d.%m.%Y %H:%M')
        formatted_date = date_object.strftime('%H.%M.%S %d.%m.%Y')
        return formatted_date
